set -e; set -v
# Update apt cache if necessary
"$(dirname "$0")/apt/update.sh"
# Install apt requirements
REQUIREMENTS=$(cat requirements/apt.txt)
# We want word splitting on these commands; disable linter rule
#shellcheck disable=SC2086
{
    # Install requirements
    apt-get install --no-install-recommends -y ${REQUIREMENTS}
    # Display the installed versions
    apt-cache policy ${REQUIREMENTS}
}
