set -e; set -v;
# Check if /var/cache/apt is older than 12 hours before doing apt-update
LAST_APT_UPDATE="$(date -r /var/cache/apt +%s)"
TIME_SINCE_UPDATE="$(( LAST_APT_UPDATE - $(date +%s) ))"
APT_UPDATE_RATE=43200 # 12 hours
if [ ${TIME_SINCE_UPDATE} -gt ${APT_UPDATE_RATE} ]; then
    apt-get update
    touch /var/apt/cache
fi
