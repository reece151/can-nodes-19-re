Directory [install](./)

This directory contains install scripts for each type of requirement we have.

If installing a particular category takes multiple steps, there will be a sub-directory as well.
