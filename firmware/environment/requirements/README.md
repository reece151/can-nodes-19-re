Directory [requirements](./)

This directory contains plain-text requirements files.

They can be for the native package manager, for a language specific package manager,
and for those third party tools that can't be installed via a package manager.

In the case of the [other.txt](./other.txt) file, each entry needs a matching script in
[../scripts/install/other](../scripts/install/other).
