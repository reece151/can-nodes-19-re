/**
 * Generic ring buffer API
 * Using only static allocations, suitable for embedded devices
 *
 * Author(s): Reece Kibble
 */

#include "cmt_ringbuffer.h"

/* -- PRIVATE FUNCTIONS -- */
void advance_head(rbuf_handle_t* rbuf) {
    if(rbuf->full) {
        rbuf->tail = (rbuf->tail + 1) % rbuf->max;
    }

    rbuf->head = (rbuf->head + 1) % rbuf->max;
    // We mark full because we will advance tail on the next time around
    rbuf->full = (rbuf->head == rbuf->tail);
}

void advance_tail(rbuf_handle_t* rbuf) {
    rbuf->full = 0; // false
    rbuf->tail = (rbuf->tail + 1) % rbuf->max;
}

/* -- API --- */
void ringbuf_init(rbuf_handle_t* rbuf, RINGBUFFER_DATATYPE* buffer, uint32_t size) {
	rbuf->buffer = buffer;
	rbuf->max = size;
    rbuf->active = 1;
	ringbuf_reset(rbuf);
}

void ringbuf_deinit(rbuf_handle_t* rbuf) {
    rbuf->active = 0;
}

void ringbuf_reset(rbuf_handle_t* rbuf) {
    rbuf->head = 0;
    rbuf->tail = 0;
    rbuf->full = 0; // false
}

uint32_t ringbuf_size(rbuf_handle_t* rbuf) {
	uint32_t size = rbuf->max;

	if(!rbuf->full) {
		if(rbuf->head >= rbuf->tail) {
			size = (rbuf->head - rbuf->tail);
		}
		else {
			size = (rbuf->max + rbuf->head - rbuf->tail);
		}
	}

	return size;
}

uint32_t ringbuf_capacity(rbuf_handle_t* rbuf) {
	return rbuf->max;
}

int8_t ringbuf_put(rbuf_handle_t* rbuf, RINGBUFFER_DATATYPE data) {
    int8_t r = -1;
    
    if(rbuf->active) {
        rbuf->buffer[rbuf->head] = data;
        advance_head(rbuf);
        r = 0;
    }

    return r;
}

int8_t ringbuf_putnfull(rbuf_handle_t* rbuf, RINGBUFFER_DATATYPE data) {
    int8_t r = -1;

    if(rbuf->active){
        if(!ringbuf_full(rbuf)) {
            rbuf->buffer[rbuf->head] = data;
            advance_head(rbuf);
            r = 0;
        }
    }

    return r;
}

int8_t ringbuf_get(rbuf_handle_t* rbuf, RINGBUFFER_DATATYPE* data) {
    int8_t r = -1;

    if(rbuf->active) {
        if(!ringbuf_empty(rbuf)) {
            if(data)
                *data = rbuf->buffer[rbuf->tail];

            advance_tail(rbuf);

            r = 0;
        }
    }

    return r;
}

uint8_t ringbuf_empty(rbuf_handle_t* rbuf) {
    return (!rbuf->full && (rbuf->head == rbuf->tail));
}

uint8_t ringbuf_full(rbuf_handle_t* rbuf) {
    return rbuf->full;
}

