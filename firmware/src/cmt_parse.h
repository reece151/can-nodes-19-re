/**
 * G-Code inspired simple parser
 * 
 * Command format:
 * <letter address><command identifier> <param1><val1> <param2><val2> <param3><val3> <param4><val4> <param5><val5>
 *
 * A parameter consists of a letter followed by a value. Up to 5 parameters are supported 
 *  
 * The list of commands should be documented by the user in the external command processing function
 * 
 * Author(s): Reece Kibble
 */

#include <stdint.h>

/**
 * Parameter structure of letter (ch) and value
 */
typedef struct {
    uint8_t ch;
    uint64_t val;
} param_t;

/**
 * Command structure of letter (ch), id, and 5 parameters
 */
typedef struct {
    uint8_t ch;
    uint64_t id;
    param_t param[5];
} command_t;

/**
 * Parse a string into a command structure.
 * Invalid strings will return as empty structures.
 */
command_t parse_command(uint8_t* cmdstr);