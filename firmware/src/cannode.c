
#include "main.h"

#include "adc.h"
#include "can.h"
#include "dma.h"
#include "usart.h"
#include "usb_device.h"
#include "usbd_cdc_if.h"
#include "gpio.h"

#include "cmt_utils.h"
#include "cmt_flash.h"
#include "cmt_usbcdc.h"
#include "cmt_ringbuffer.h"
#include "cmt_parse.h"

extern void SystemClock_Config(void);

typedef union {
    uint16_t u16;
    uint8_t u8[2];
} can_data_t;

/* Note: adc_buff_len/2 must be divisible by 4, 3 and 2. (therefore minimum value is 12 for halflen) */
#define adc_buff_len 2592
static uint16_t adc_buff_halflen;

// Create ADC buffer
uint16_t adc_buff[adc_buff_len] = {0};

// Pointer to the active half of the DMA buffer
static uint16_t *active_buffer = adc_buff;
static bool_t buffer_new = FALSE;

static flash_t flash = {};
static usbcdc_t usbcdc = {};

void flash_default_values(flash_t *flash) {
    // All channels active
    flash->program.adc_channel_A = 1;
    flash->program.adc_channel_B = 1;
    flash->program.adc_channel_C = 1;
    flash->program.adc_channel_D = 1;
    flash->program.can_id = 0x7FF; // Can ID 2047 (maximum)
    flash->program.can_hz = can_200hz; // 200Hz transmit frequency (maximum)
}

#define adc_channel_total_count 4
#define adc_channel_active(_index) ({ \
    ((*(uint32_t*)&flash.program >> (_index + 15)) & 0x1); \
})
#define adc_channel_active_count ({ \
    adc_channel_active(0) + /* Channel A */ \
    adc_channel_active(1) + /* Channel B */ \
    adc_channel_active(2) + /* Channel C */ \
    adc_channel_active(3);  /* Channed D */ \
})

/**
 * @brief  Initialise the ADC channels and ringbuffers, and begin DMA sampling
 */
static void cannode_init_channels(void) {
    ADC_ChannelConfTypeDef sConfig = {0};
    sConfig.SamplingTime = ADC_SAMPLETIME_41CYCLES_5;

    HAL_ADC_Stop_DMA(&hadc);

    /* Initialise/Disable channels (note this is overriding some of MX_ADC_Init) */
    for(uint8_t i = 0; i < adc_channel_total_count; i++){
        sConfig.Channel = i;
        /* Create channel ringbuffer */
        if(adc_channel_active(i)) {
            sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
        }
        else {
            sConfig.Rank = ADC_RANK_NONE;
        }

        if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK) {
            Error_Handler();
        }
    }

    /* Calibrate ADC */
    if (HAL_ADCEx_Calibration_Start(&hadc) != HAL_OK) {
        Error_Handler();
    }
    /* Begin ADC sampling to DMA */
    if (HAL_ADC_Start_DMA(&hadc, (uint32_t*)adc_buff, adc_buff_len) != HAL_OK) {
        Error_Handler();
    }
}

/**
 * @brief   Perform LED blink by incrementing varaible and toggling the LED on the overflow
 */
static void cannode_led_blink(void) {
    static uint8_t led_slowblink = 0;

    led_slowblink += 8;
    if(led_slowblink == 0)
        HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
}

/**
 * @brief   Transmit a CAN message from the transmit ring buffer.
 *          Transmissions done in groups of three to match the number of CAN mailboxes
 */
static void cannode_transmit(can_data_t* data) {
    static CAN_TxHeaderTypeDef cantx_h = {
        .IDE = CAN_ID_STD,
        .RTR = CAN_RTR_DATA,
        .DLC = 8,
    };

    cantx_h.StdId = flash.program.can_id;

    uint8_t tx_bytes[8];

    // Convert to CAN endian-ness
    tx_bytes[0] = data[0].u8[0];
    tx_bytes[1] = data[0].u8[1];
    tx_bytes[2] = data[1].u8[0];
    tx_bytes[3] = data[1].u8[1];
    tx_bytes[4] = data[2].u8[0];
    tx_bytes[5] = data[2].u8[1];
    tx_bytes[6] = data[3].u8[0];
    tx_bytes[7] = data[3].u8[1];

    // Mailbox unused here, assumed that the CAN rate is slower than ADC
    // 8 bytes transmitted, specified by DLC
    static uint32_t mailbox;
    if (HAL_CAN_AddTxMessage(&hcan, &cantx_h, tx_bytes, &mailbox) != HAL_OK) {
        Error_Handler();
    }
}

static void cannode_process_command(usbcdc_t *usbcdc, command_t command) {
    switch (command.ch) {
        case 'I': {
            switch (command.id) {
                case 0: // I00 - Initialise CAN Node communication
                    usbcdc->echo = TRUE;
                    usb_command_processed(usbcdc);
                    break;
                case 1: // I10 E<echo state> - Toggle echo state
                    if (command.param->ch == 'E') {
                        usbcdc->echo = (command.param->val & 0x1);
                        usb_command_processed(usbcdc);
                    }
                    break;
                default:
                    break;
            }
            break;
        }
        case 'P': {
            switch (command.id) {
                case 0: // P00 I<can ID> - Set CAN ID
                    if (command.param->ch == 'I') {
                        flash.program.can_id = (command.param->val & 0x7FF);
                        usb_command_processed(usbcdc);
                    }
                    break;
                case 1: // P01 F<can freq> - Set CAN transmit frequency
                    if (command.param->ch == 'F') {
                        switch (command.param->val) {
                            case 200:
                                flash.program.can_hz = can_200hz;
                                usb_command_processed(usbcdc);
                                break;
                            case 100:
                                flash.program.can_hz = can_100hz;
                                usb_command_processed(usbcdc);
                                break;
                            case 50:
                                flash.program.can_hz = can_50hz;
                                usb_command_processed(usbcdc);
                                break;
                            case 25:
                                flash.program.can_hz = can_25hz;
                                usb_command_processed(usbcdc);
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case 2: { // P02 A<channel state> B<channel state> C<channel state> D<channel state> - Enable/Disable ADC channels
                    for (uint8_t i = 0; i < 4; i++) {
                        switch(command.param[i].ch) {
                            case 'A':
                                flash.program.adc_channel_A = (command.param[i].val & 0x1);
                                usb_command_processed(usbcdc);
                                break;
                            case 'B':
                                flash.program.adc_channel_B = (command.param[i].val & 0x1);
                                usb_command_processed(usbcdc);
                                break;
                            case 'C':
                                flash.program.adc_channel_C = (command.param[i].val & 0x1);
                                usb_command_processed(usbcdc);
                                break;
                            case 'D':
                                flash.program.adc_channel_D = (command.param[i].val & 0x1);
                                usb_command_processed(usbcdc);
                                break;
                            default:
                                break;
                        }
                    }
                    cannode_init_channels();
                }
                    break;
                case 95: // P95 - Save the configuration
                    flash_write(&flash);
                    usb_command_processed(usbcdc);
                    break;
                case 96: // P96 - Reset configuration to default
                    flash_default_values(&flash);
                    usb_command_processed(usbcdc);
                    break;
                case 97: // P97 - Restore configuration from last save
                    flash_read(&flash);
                    usb_command_processed(usbcdc);
                    break;
                case 98: // P98 - Erase and reset configuration
                    flash_erase();
                    flash_init(&flash);
                    usb_command_processed(usbcdc);
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}


/**
 * @brief   CAN-Nodes main program loop
 */
int main(void) {
    /* HAL initialisation */
    HAL_Init();

    /* Configure system clock */
    SystemClock_Config();

    /* MX initialisations */
    MX_GPIO_Init();
    MX_DMA_Init(); /* Initialise DMA before ADC */
    MX_ADC_Init();
    MX_CAN_Init();
    MX_USB_DEVICE_Init();

    flash_init(&flash);
    usb_init(&usbcdc);

    HAL_CAN_Start(&hcan);

    static uint32_t adc_channel_total[4] = {0};
    static uint16_t adc_samples_per_channel = 0;

    static can_hz_t ma_hz = can_200hz; // The frequency of the moving average
    static can_data_t data[4];

    adc_buff_halflen = adc_buff_len/2;
    adc_samples_per_channel = adc_buff_halflen/adc_channel_active_count;

    /* Initialise ADC channel configuration and buffers */
    cannode_init_channels();

    /* Inifinite loop */
    while(1) {
        // Poll buffer status
        if (buffer_new == TRUE) {
            uint8_t k = 0;

            // Handle ADC channel data and add resuslts to transmit buffer
            for (uint8_t i = 0; i < adc_channel_total_count; i ++) {
                adc_channel_total[i] = 0;
                data[i].u16 = 0;

                if (adc_channel_active(i)) {
                    for (uint16_t j = 0; j < adc_samples_per_channel; j++) {
                        adc_channel_total[i] += active_buffer[k + j*adc_channel_active_count];
                    }
                    // Moving average
                    data[i].u16 = (data[i].u16 + adc_channel_total[i]/adc_samples_per_channel)/2;
                    k++;
                }
            }

            // Transmit data if the moving average frequency matches the desired can frequency
            // Moving average frequency (effective frequncy of channel data) is reduced on every non-transmitted loop
            // 200hz -> 100hz -> 50hz -> 25hz (minimum)
            if(ma_hz == flash.program.can_hz) {
                cannode_transmit(data);
                cannode_led_blink(); // Blink led relative to can transmit frequency
                ma_hz = can_200hz; // Reset moving average frequency

                data[0].u16 = 0; // Reset transmit data
                data[1].u16 = 0;
                data[2].u16 = 0;
                data[3].u16 = 0;
            }
            else {
                ma_hz++;
            }

            // Reset buffer read status
            buffer_new = FALSE;
        }

        // Poll for USB command
        if (usbcdc.command_ready == TRUE) {
            command_t command = parse_command(usbcdc.buf_command);
            cannode_process_command(&usbcdc, command);
            usbcdc.command_ready = FALSE;
        }
    }
}

/**
 * @brief   Using HalfCplt and Cplt callbacks to operate on the half of the
 *          buffer which is not being iterated by the DMA
*/
void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef* hadc) {
    active_buffer = adc_buff;
    buffer_new = TRUE;
}
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc) {
    active_buffer = adc_buff + adc_buff_halflen;
    buffer_new = TRUE;
}
