/**
 * For interacting over USB CDC
 * Author(s): Reece Kibble
 */

#ifndef _CMT_USBCDC_H
#define _CMT_USBCDC_H

#include <stdint.h>
#include "usbd_cdc_if.h"
#include "cmt_ringbuffer.h"
#include "cmt_utils.h"

/**
 * Structure for USB CDC data transfer and control
 */
typedef struct usbcdc_t {
    // Receive ringbuffer
    rbuf_handle_t hrbuf_rx;
    uint8_t buf_rx[64];
    uint8_t buf_command[64];
    bool_t command_ready;
    bool_t echo;
} usbcdc_t;

/**
 * Call this function after MX_USB_DEVICE_Init to initialise the USB communication
 */
void usb_init(usbcdc_t *usbcdc);

/**
 * Receive a string from USB and set flag if a command has been received.
 * Call this from usbd_cdc_if.c
 */
void usb_receive(usbcdc_t *usbcdc, uint8_t *buf, uint32_t *len);

/**
 * Report over USB that the command was processed successfully
 */
void usb_command_processed(usbcdc_t *usbcdc);


#endif