/**
 * For using STM32 flash as non-volatile storage
 * NOT emulating EEPROM, flash is flash!
 * Supports page switching and page sub-sectioning for wear distribution.
 *
 * Author(s): Reece Kibble
 */

#ifndef _CMT_FLASH_H
#define _CMT_FLASH_H

#include "cmt_utils.h"

/******* Micro specific configuration *********/
// The address at which program written memory will start
#define FLASH_SECTOR_START 0x0800F000 // flash sector 15
// The maximum of addresses within the sector
#define FLASH_SECTOR_MAX 0xFFF
// The number of pages in the sector of the flash memory
#define FLASH_PAGE_IN_SECTOR 2
/******** END Micro specific configuration *********/

/******* Project specific configuration *******/
typedef enum {
    can_200hz = 1,
    can_100hz = 2,
    can_50hz = 4,
    can_25hz = 8
} can_hz_t;

/** IMPORTANT: Must be a size multiple of uint32_t! */
typedef __PACKED_STRUCT {
    // Store the CAN ID of the device
    uint32_t can_id : 11;
    // Store the CAN transmit frequency of the device
    can_hz_t can_hz : 4;
    // Store the device's active channel configuration
    uint32_t adc_channel_A : 1;
    uint32_t adc_channel_B : 1;
    uint32_t adc_channel_C : 1;
    uint32_t adc_channel_D : 1;
    // Reserved bits 19 to 31
    uint32_t _reserved_19 : 13;
} program_flash_t;
/********** END project specific configuration *******/

#define FLASH_SECTION_SIZE (sizeof(program_flash_t))

typedef union {
    program_flash_t program;
    uint8_t _bytes_[FLASH_SECTION_SIZE];
} flash_t;

/**
 * This function must be called before flash_write or flash_page_swap
 * Finds the active page in the sector.
 */
void flash_init(flash_t *flash);

/**
 * Reads the flash into the bitfield
 */
void flash_read(flash_t *flash);

/**
 * Write the given address for data.
 * Calls flash_check to make sure the addresses are pre-erased, if not then
 * write to the next page.
 */
void flash_write(flash_t *flash);

/**
 * Erase all storage flash
 */
void flash_erase(void);

#endif