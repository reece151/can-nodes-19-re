/**
 * G-Code inspired simple parser
 * 
 * Command format:
 * <letter address><command identifier> <param1><val1> <param2><val2> <param3><val3> <param4><val4> <param5><val5>\r
 *
 * A parameter consists of a letter followed by a value. Up to 5 parameters are supported 
 *  
 * The list of commands should be documented by the user in the external command processing function
 * 
 * Author(s): Reece Kibble
 */

#include "cmt_utils.h"
#include "cmt_parse.h"

command_t parse_command(uint8_t* cmdstr) {
    // Initialise empty command
    command_t command = {};

    // Skip whitespaces
    while (*cmdstr == ' ') ++cmdstr;
    
    if (utils_isalpha(*cmdstr)) {
        command.ch = utils_uppercase(*cmdstr++);
        
        // Skip whitespaces to ID (usally none)
        while (*cmdstr == ' ') ++cmdstr;

        // Get command ID
        do {
            command.id *= 10,
            command.id += *cmdstr++ - '0';
        } while ((*cmdstr >= '0') && (*cmdstr <= '9'));
    }
    else {
        return command;
    }
    
    // Skip whitespaces to first argument
    while (*cmdstr == ' ') ++cmdstr;

    uint8_t i = 0;
    while (utils_isalpha(*cmdstr)) {
        // Get parameter character
        command.param[i].ch = utils_uppercase(*cmdstr++);
        
        // Skip whitespaces between parameters and values (usally none)
        while (*cmdstr == ' ') ++cmdstr;

        // Get parameter value
        do {
            command.param[i].val *= 10,
            command.param[i].val += *cmdstr++ - '0'; 
        } while ((*cmdstr >= '0') && (*cmdstr <= '9'));
        
        // Skip whitespaces between parameters
        while (*cmdstr == ' ') ++cmdstr;

        i++;
    }

    return command;
}