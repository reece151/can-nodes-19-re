/**
 * Handy preprocessor macros and utility functions for use with embedded programs
 * Author(s): Reece Kibble
 */

#include "stm32f0xx.h"

#include "cmt_utils.h"

void utils_reverse(uint8_t* str, uint16_t len) {
    for (uint16_t i = 0; i < len/2; i++) {
        SWAP(*(str + i), *(str + (len - 1) - i));
    }
}

uint8_t* utils_itoa(int64_t num, uint8_t* str) {
    uint8_t i = 0;
    int64_t sign;

    str[i++] = '\0'; // Append string terminator
    
    if ((sign = num) < 0) {
        num = -num;
    }

    do {
        str[i++] = num % 10 + '0';
    } while((num /= 10) > 0);

    // If number is negative, append '-'
    if (sign < 0) {
        str[i++] = '-';
    }

    // Reverse the string
    utils_reverse(str, i);

    return str;
}

int64_t utils_atoi(uint8_t* str) {
    int64_t result = 0;
    uint8_t sign = 0;
    uint8_t i = 0;

    // Getting the sign
    if (*str == '-') {
	    sign = 1;
	    i = 1;
    }
    else {
	    if (*str == '+') {
	        i = 1;
	    }
    }

    // Converting to digits
    for(; (str[i] >= '0') && (str[i] <= '9') ; i++) {
	    result = (10*result) + str[i];
    }

    if (sign) {
	    return -result;
    }
    return result;
}

uint8_t utils_uppercase(uint8_t c) {
    if ( (c >= 'a') && (c <= 'z') ) {
        c += 'A' - 'a';
    }
    return c;
}

bool_t utils_isalpha(uint8_t c) {
    uint8_t d = utils_uppercase(c);
    if ( (d >= 'A') && (d <= 'Z') ) {
        return TRUE;
    }
    return FALSE;
}

bool_t utils_isnum(uint8_t c) {
    if ( (c >= '0') && (c <= '9') ) {
        return TRUE;
    }
    return FALSE;
}