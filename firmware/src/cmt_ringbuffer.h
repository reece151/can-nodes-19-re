/**
 * Generic ring buffer API
 * Using only static allocations, suitable for embedded devices
 *
 * Author(s): Reece Kibble
 */

#ifndef _RINGBUFFER_H
#define _RINGBUFFER_H

#include <stdint.h>

#define RINGBUFFER_DATATYPE uint8_t

/**
 * Handle type, for the user to interact with the API
 */
typedef struct _rbuf_handle_t {
	RINGBUFFER_DATATYPE* buffer;
	uint32_t head;
	uint32_t tail;
	uint32_t max; //of the buffer
	uint8_t full;
	uint8_t active;
} rbuf_handle_t;

/**
 *  Pass in a storage buffer and size to initialise
 */
void ringbuf_init(rbuf_handle_t* rbuf, RINGBUFFER_DATATYPE* buffer, uint32_t size);

/**
 * De-initialise the ringbuf (marks inactive) 
 */
void ringbuf_deinit(rbuf_handle_t* rbuf);

/**
 * Reset the circular buffer to empty, head == tail
 */
void ringbuf_reset(rbuf_handle_t* rbuf);

/**
 * Put continues to add data if the buffer is full.
 * Advances the buffer pointer.
 * Old data is overwritten
 * Returns 0 on success, -1 otherwise
 */
int8_t ringbuf_put(rbuf_handle_t* rbuf, RINGBUFFER_DATATYPE data);

/**
 * Putnfull rejects new data if the buffer is full.
 * Advances the buffer pointer.
 * Returns 0 on success, -1 if buffer is full
 */
int8_t ringbuf_putnfull(rbuf_handle_t* rbuf, RINGBUFFER_DATATYPE data);

/**
 * Retrieve a value from the buffer.
 * Retreats the buffer pointer.
 * Returns 0 on success, -1 if the buffer is empty
 */
int8_t ringbuf_get(rbuf_handle_t* rbuf, RINGBUFFER_DATATYPE* data);

/**
 * Returns 1 if the buffer is empty
 */
uint8_t ringbuf_empty(rbuf_handle_t* rbuf);

/**
 * Returns 1 if the buffer is full
 */
uint8_t ringbuf_full(rbuf_handle_t* rbuf);

/**
 * Returns the maximum capacity of the buffer
 */
uint32_t ringbuf_capacity(rbuf_handle_t* rbuf);

/**
 * Returns the current number of elements in the buffer
 */
uint32_t ringbuf_size(rbuf_handle_t* rbuf);

#endif