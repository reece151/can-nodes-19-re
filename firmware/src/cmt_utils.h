/**
 * Handy preprocessor macros and utility functions for use with embedded programs
 * Author(s): Reece Kibble
 */

#ifndef _CMT_UTILS_H
#define _CMT_UTILS_H

#include <stdint.h>

typedef enum {
    FALSE = 0,
    TRUE = !FALSE
} bool_t;

// Shift the value by n bytes
// eg. BYTESHIFT(0x08, 1), 1000 -> 1000 0000 0000
#define BYTESHIFT(val, n) ((val) << ((n)*8))

// Get the specified bit out of multiple bits of data. Results in 1 or 0.
// eg. GETBIT(0xA1, 3) -> 1010 [0]001 = 0
#define GETBIT(val, n) (((val) >> (n)) & 0x01)

// Get a power of two by shifting left n times.
// eg. POW2(4) -> 0001 0000 = 16 ( = 2^4 )
#define POW2(n) (0x01 << (n))

// Swap the values of a and b.
#define SWAP(a,b) { \
    (a) ^= (b);     \
    (b) ^= (a);     \
    (a) ^= (b);     \
}

/**
 * Reverses the given string of length len
 */
void utils_reverse(uint8_t* str, uint16_t len);

/**
 * Lightweight implementation of itoa. Converts an integer to a string. A string buffer has to be supplied
 * to the str pointer, and the pointer to this string is also returned.
 */
uint8_t* utils_itoa(int64_t num, uint8_t* str);

/**
 * Lightweight implementation of atoi. Converts a string to an integer. Requires pointer to the string
 */
int64_t utils_atoi(uint8_t* str);

/**
 * Converts lowercase character to uppercase, returning the uppercase result
 * If the character is not a lowercase alphabetical character, it is returned as is
 */
uint8_t utils_uppercase(uint8_t c);

/**
 * Checks an integer represents an ASCII alphabetical character (a-z or A-Z)
 */
bool_t utils_isalpha(uint8_t c);

/**
 * Checks an integer represents an ASCII numerical character (0-9)
 */
bool_t utils_isnum(uint8_t c);

#endif