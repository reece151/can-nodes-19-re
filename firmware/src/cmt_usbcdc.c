/**
 * For interacting over USB CDC
 * Author(s): Reece Kibble
 */

#include "cmt_usbcdc.h"

#define usb_send_message(_msg) ({ \
    CDC_Transmit_FS((uint8_t*)(_msg), sizeof((_msg))/sizeof(uint8_t)); \
})

void usb_init(usbcdc_t *usbcdc) {
    usbcdc->echo = FALSE;
    usbcdc->command_ready = FALSE;

    // USB receive ringbuffer
    ringbuf_init(&usbcdc->hrbuf_rx, usbcdc->buf_rx, 64);
}

void usb_receive(usbcdc_t *usbcdc, uint8_t *buf, uint32_t *len) {
    for (uint8_t i = 0; i < *len; i++) {
        ringbuf_put(&usbcdc->hrbuf_rx, buf[i]);

        if (buf[i] == '\r') { // Command received, copy the ringbuffer to the command buffer
            for (uint8_t j = 0; !ringbuf_empty(&usbcdc->hrbuf_rx); j++) {
                ringbuf_get(&usbcdc->hrbuf_rx, &usbcdc->buf_command[j]);
            }
            // Set flag for use in main loop
            usbcdc->command_ready = TRUE;
        }
    }
}

inline void usb_command_processed(usbcdc_t *usbcdc) {
    if (usbcdc->echo == TRUE) {
        usb_send_message("OK\r\n");
    }
}