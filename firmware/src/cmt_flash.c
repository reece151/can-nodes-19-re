/**
 * For using STM32 flash as non-volatile storage
 * NOT emulating EEPROM, flash is flash!
 * Supports page switching and page sub-sectioning for wear distribution.
 *
 * Operation:
 *  Using flash has its drawbacks. Flash has to be erased before being re-written,
 *  but the circuitry only allows for erasing pages of flash at a time. Flash also has
 *  very limited write cycles (10 k) compared to EEPROM (1 million) or NVRAM (1 trillion).
 *  These limitations don't really affect flash in its intended use case as program instruction storage
 *  since a program is usually erased and re-written in large chunks of data and does not happen very often.
 *
 *  cmt_flash skirts around these problems by reserving a sector of flash and cycling the data through the pages
 *  in the sector (Page Switching). This avoids the issue of the page erase granularity and also increases potential write cycles by
 *  distributing the wear on the pages throughout the sector. The disadvantage is that at least two pages must be reserved
 *  for data that would usually take only a single page.
 *
 *  cmt_flash also uses Sub-Sectioning to further reduce wear. A page contains 1kb of data, but often the application does not
 *  require that much non-volatile storage. Thus, the user describes the bitfield program_flash_t which contains all the required
 *  variables to store. This bitfield must be word aligned. Sub-sectioning divides the active page into sections that each fit the
 *  bitfield. Thus a page can be used multiple times to store the same data, which in cases where program_flash_t is small, can greatly
 *  reduce wear.
 *
 *  For example: The user wants to store 128 bytes of data. The flash page size is 1kb and they reserve sector 7
 *  for the program data, which contains four pages. Thus they use the sector for their 1kb of data. The user defines program_flash_t
 *  with their 128 bytes, thus allowing each 1kb page to be divided into (1024/128) 8 sub-sections.
 *  NOTE: The page size/sector size claimed above is an example. Check your micro-specific sizes!
 *
 *  The basic operation for this example is as follows:
 *      The user sets variables to store in a program_flash_t bitfield structure.
 *          If there is no data already at the requested address, write the bitfield to the address.
 *          If there is data already at the requested address, check the next sub-section
 *              If there are no unwritten sub-sections, check the next page.
 *                  Write the bitfield to the address
 *
 * Author(s): Reece Kibble
 */

#include "stm32f0xx.h"
#include "stm32f0xx_hal_flash.h"
#include "cmt_flash.h"

#define FLASH_PAGE_ADDR(q) (FLASH_SECTOR_START + FLASH_PAGE_SIZE*(q))
#define FLASH_SECTION_COUNT (FLASH_PAGE_SIZE/FLASH_SECTION_SIZE)
#define FLASH_ACTIVE_ADDR (FLASH_PAGE_ADDR(flash_active_page) + FLASH_SECTION_SIZE*flash_active_section)

uint8_t flash_active_page;
uint16_t flash_active_section;

// Project specific default values
extern void flash_default_values(flash_t *flash);

/**
 * Erases the page(s) from the given address.
 * This erases an ENTIRE PAGE which is the lower limit of the erase granularity.
 * uint32_t address - the start address of the page to be erased.
 * uin8_t n - the number of pages to erase (minimum n = 1)
 */
static void erase_page(uint32_t address, uint8_t n){
    // Error indicator. Defaults to 0xFFFFFFFF on success, or the failing address on error.
    uint32_t page_error = 0;

    // Erase struct is passed as argument of FLASHEx_Erase
    FLASH_EraseInitTypeDef  eraseInit;
    eraseInit.TypeErase = FLASH_TYPEERASE_PAGES;
    eraseInit.PageAddress = address;
    eraseInit.NbPages = n;

    HAL_FLASH_Unlock();

    HAL_FLASHEx_Erase(&eraseInit, &page_error);

    #ifdef FLASH_DEBUG
        if(page_error != 0xFFFFFFFF) {
            char msg[] = "E: Page erase failed\n";
            debug_print(msg, sizeof(msg)/sizeof(char)-1);
        }
    #endif

    HAL_FLASH_Lock();
}

// check to see if any words at the active location have been written
static uint8_t check_written(flash_t *flash) {
    for(uint16_t i = 0; i < FLASH_SECTION_SIZE; i++) {
        if (*(uint8_t*)(FLASH_ACTIVE_ADDR+i) != 0xFF) {
            return 1;
        }
    }
    return 0;
}

// Get the first free sub-section
static void get_free_section(flash_t *flash) {
    // Determine correct active address (first free section)
    while(check_written(flash)) {
        // check next section
        flash_active_section++;
        if (flash_active_section < FLASH_SECTION_COUNT) {
            continue;
        }
        else { // All sections exchausted
            flash_active_section = 0;
            flash_active_page++;
            if (flash_active_page == FLASH_PAGE_IN_SECTOR) {
                flash_active_page = 0; // All pages exhausted
            }
        }
    }
}

void flash_init(flash_t *flash) {
    // no active page
    flash_active_page = 0xFF;
    flash_active_section = 0;

    // Determine active page
    for(uint8_t i = 0; i < FLASH_PAGE_IN_SECTOR; i++) {
        if(*(uint32_t*)FLASH_PAGE_ADDR(i) != 0xFFFFFFFF) {
            flash_active_page = i; // non-0xFF found, therefore active sector
        }
    }

    if (flash_active_page != 0xFF) {
        // Determine active section (Section before the free section)
        get_free_section(flash);
        flash_active_section--;
        flash_read(flash);
    }
    else {
        flash_active_page = 0;

        flash_default_values(flash);
    }
}

void flash_read(flash_t *flash) {
    for(uint16_t i = 0; i < FLASH_SECTION_SIZE/sizeof(uint32_t); i += 4) {
        *(uint32_t*)(flash->_bytes_ + i) = *(uint32_t*)(FLASH_ACTIVE_ADDR + i);
    }
}

void flash_write(flash_t *flash) {
    HAL_FLASH_Unlock();

    uint8_t flash_cached_page = flash_active_page;

    get_free_section(flash);

    if (flash_cached_page != flash_active_page) {
        erase_page(FLASH_PAGE_ADDR(flash_cached_page), 1);
    }

    // Write flash
    for(uint16_t i = 0; i < FLASH_SECTION_SIZE; i += 4) {
        HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, FLASH_ACTIVE_ADDR + i, *(uint32_t*)(flash->_bytes_ + i));
    }

    HAL_FLASH_Lock();
}

inline void flash_erase(void) {
    erase_page(FLASH_SECTOR_START, FLASH_PAGE_IN_SECTOR);
}
