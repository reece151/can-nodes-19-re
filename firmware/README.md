This folder contains the firmware for the can-nodes-19 project.

## Installation
The following updated steps have been tested on a clean Debian Bullseye installation.<br>
The machine must be connected to a display... cubeMX installation is graphical.

These updated steps have been made with reference to the existing (archived?) steps on the CMT connie page *"Basic Programming Setup (& FAQ)"*.

### Install WSL (Only required on Windows)
Windows Subsystem for Linux is required if you want to stay on a Windoze primary OS.<br>
Please refer to the documentation on Confluence: *"Setting up a UNIX Environment"* (I think thats what I called it?)

### Setup WSL for GUI apps (Only required on Windows)
We need to be able to run STM32CubeMX from our WSL terminal. And we need to see it in Windows.<br>
Install [VcXsrv](https://sourceforge.net/projects/vcxsrv/).<br>
Accept firewall permissions for **all** interfaces on the popup!<br>

The following must be repeated whenever you want to view a GUI from WSL (such as STM32CubeMX):
* Run "XLaunch" from Windows.
* In the "Display Settings" window that appears, click 'next' until you're on the "Extra Settings" page.
* Enable the "Disable access control" checkbox.
* Click through to finish the configuration.

Set the DISPLAY variable in WSL:
```sh
# Append the belo commands to your ~/.bashrc file to make the settings permanent.
# .bashrc is run on every new terminal session, so you only need to do this once.
export LOCALHOST=$(ip route list default | awk '{print $3}')
export DISPLAY=$LOCALHOST:0
export LIBGL_ALWAYS_INDIRECT=1
```
Restart your terminal to apply the settings. GUI apps should now work.

### Get STM32CubeMX
Download CubeMX for linux from [st.com](https://www.st.com/content/st_com/en/products/development-tools/software-development-tools/stm32-software-development-tools/stm32-configurators-and-code-generators/stm32cubemx.html) (you will need to make an account if you don't have one)

Then, from the terminal:
```sh
# Install the default JRE
apt install default-jre
# Install unzip if necessary
apt install unzip
# unzip the zip
unzip en.stmcubemx-lin_version.zip
# Run the installer, install with default settings
./SetupSTMCubeMX
# At this point, you can go back and delete the downloaded zip and the installer.
# CubeMX should now be installed. You can run it manually using the command:
~/STM32CubeMX/STM32CubeMX
```

### Get the GNU ARM Embedded toolchain
Download the `x86_64` Linux cross toolchain from [ARM](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/downloads).<br>
For this project we're using an `AArch32` bare-metal target (arm-none-eabi).<br>
Ensure the verison you download has this format:
```
gcc-arm-[version]-x86_64-arm-none-eabi.tar.xz
```
Next we will install the toolchain:
```sh
# Before we start, let's get the basics (includes gcc, make, etc.)
apt install build-essential gdb-multiarch
# Navigate to the place where the compressed files were downlaoded.
# Installation is as simple as extracting to /usr/local, sort of
tar -C /usr/local -xJf gcc-arm-[version]-x86_64-arm-none-eabi.tar.xz --strip-components 1
# Check the installation was successful
arm-none-eabi-gcc -v
```

### Get openocd (Linux)
The original steps work fine here. Refer to *"Basic Programming Setup (& FAQ)"*

To use OpenOCD natively,
Within `can-nodes-19-re/firmware/makefiles/build/Openocd.mk`:
```makefile
# Make sure this line is uncommented:
OPENOCD_EXEC ?= /usr/local/bin/openocd
```

### Get openocd (Windows)
To use OpenOCD from Windows Subsystem for linux, we will use the executable included with this repository.
Within `can-nodes-19-re/firmware/makefiles/build/Openocd.mk`:
```makefile
# Make sure this line is uncommented:
OPENOCD_EXEC ?= ./environment/requirements/openocd/windows-0.10.0/bin/openocd.exe
```

Make the windows application executable from Linux:
```sh
chmod +x environment/requirements/openocd/windows-0.11.0/bin/openocd.exe
```

Now we can run openocd and connect using the `make` target:
```sh
make openocd
```
Accept firewall permissions for **all** interfaces on the popup!

To test that it's working, we should now be able to connect from the WSL terminal, using GDB:
```sh
gdb-multiarch -x "target remote $LOCALHOST:3333"
```

### Get can-nodes-19
Finally, we have all we need to develop on CAN nodes.<br>
Run the below commands to prepare the project and verify everything is working:
```sh
cd can-nodes-19/firmware

# run make 'help' command to see what commands are available
# the first time make is run, the CubeMX project will regenerate. This will take a bit of time.
# CubeMX may prompt you to download a repository, this is OK, press yes.
make help

# Build the firmware, ensure there are no compilation or linker errors
make build

# Connect to the target through OpenOCD
make openocd

# Flash the target through OpenOCD
make openocd/flash
```
