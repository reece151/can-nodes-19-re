/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define GPIO_OUT4_Pin GPIO_PIN_0
#define GPIO_OUT4_GPIO_Port GPIOF
#define GPIO_OUT3_Pin GPIO_PIN_1
#define GPIO_OUT3_GPIO_Port GPIOF
#define GPIO_OUT2_Pin GPIO_PIN_2
#define GPIO_OUT2_GPIO_Port GPIOF
#define GPIO_OUT1_Pin GPIO_PIN_3
#define GPIO_OUT1_GPIO_Port GPIOF
#define GPIO_IN4_Pin GPIO_PIN_4
#define GPIO_IN4_GPIO_Port GPIOF
#define GPIO_IN2_Pin GPIO_PIN_5
#define GPIO_IN2_GPIO_Port GPIOF
#define GPIO_IN3_Pin GPIO_PIN_6
#define GPIO_IN3_GPIO_Port GPIOF
#define GPIO_IN1_Pin GPIO_PIN_7
#define GPIO_IN1_GPIO_Port GPIOF
#define LED_Pin GPIO_PIN_8
#define LED_GPIO_Port GPIOF
#define ANLG_4_Pin GPIO_PIN_0
#define ANLG_4_GPIO_Port GPIOC
#define ANLG_1_Pin GPIO_PIN_1
#define ANLG_1_GPIO_Port GPIOC
#define ANLG_3_Pin GPIO_PIN_2
#define ANLG_3_GPIO_Port GPIOC
#define ANLG_2_Pin GPIO_PIN_3
#define ANLG_2_GPIO_Port GPIOC
#define USART1_TX_Pin GPIO_PIN_0
#define USART1_TX_GPIO_Port GPIOA
#define USART1_RX_Pin GPIO_PIN_1
#define USART1_RX_GPIO_Port GPIOA
#define USB_DM_Pin GPIO_PIN_14
#define USB_DM_GPIO_Port GPIOB
#define USB_DP_Pin GPIO_PIN_15
#define USB_DP_GPIO_Port GPIOB
#define CAN_RX_Pin GPIO_PIN_11
#define CAN_RX_GPIO_Port GPIOA
#define CAN_TX_Pin GPIO_PIN_12
#define CAN_TX_GPIO_Port GPIOA
#define SYS_SWDIO_Pin GPIO_PIN_13
#define SYS_SWDIO_GPIO_Port GPIOA
#define SYS_SWCLK_Pin GPIO_PIN_14
#define SYS_SWCLK_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
