# Openocd.mk - Makefile of targets for running openocd to control a target
# Openocd is an open source On-Chip Debugger for embedded debugging and programming
# Author(s): Reece Kibble

# Windows Subsystem for Linux users, uncomment this to use the included OpenOCD executable
OPENOCD_EXEC ?= ./environment/requirements/openocd/windows-0.11.0/bin/openocd.exe
# Native Linux users, uncomment this:
# OPENOCD_EXEC ?= /usr/local/bin/openocd

# Default OpenOCD configuration
OPENOCD_CFG ?= -f interface/stlink.cfg -f target/stm32f4x.cfg

PHONY: openocd
openocd: ## Launch OpenOCD and connect to target
openocd: $(OPENOCD_EXEC) $(SETTINGS_FILE)
	$(QUIET)
	$(if $(OPENOCD_CFG),,$(error OpenOCD configuration not set! please set with make opeonocd OPENOCD_CFG='your configuration here'))
	$(OPENOCD_EXEC) $(OPENOCD_CFG) -c "bindto 0.0.0.0" >$(LOGDIR)$@.log

PHONY: openocd/flash
openocd/flash: ## Launch OpenOCD and flash the target
openocd/flash: build $(OPENOCD_EXEC) $(SETTINGS_FILE)
	$(QUIET)
	$(if $(OPENOCD_CFG),,$(error OpenOCD configuration not set! please set with make opeonocd OPENOCD_CFG='your configuration here'))
	$(OPENOCD_EXEC) $(OPENOCD_CFG) -c "bindto 0.0.0.0 program $(HEX_FILE) verify reset exit"

SETTINGS_VARS += \
	OPENOCD_CFG \

 ## 