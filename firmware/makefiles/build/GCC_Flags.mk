# GCC_Flags.mk - Application specific GCC flags to be modified by the user

####################### C SPECIFIC #########################
# C Sources
C_SOURCES += \
	src/cannode.c \
	src/cmt_flash.c \
	src/cmt_usbcdc.c \
	src/cmt_ringbuffer.c \
	src/cmt_utils.c \
	src/cmt_parse.c

# C Pre-processor definitions
C_DEF_FLAGS +=

# C include paths (Must be preappended with -I)
# These should be minimal, or none, if you follow the recommened project structure!
# ie. The c file/header file pair should both be in the same directory.
# eg. -I stm32cubemx/Core/Inc
C_INC_FLAGS +=

# Miscellaneous C flags
CFLAGS += \
	-Werror \
    -Wall -fdata-sections -ffunction-sections

#################### ASSEMBLY SPECIFIC #####################
# Assembly Sources
AS_SOURCES +=

# Assembly Definitions
AS_DEF_FLAGS +=
# Assembly include paths (Must be preappended with -I)
# These should be minimal, or none, if you follow the recommened project structure!
# ie. The c file/header file pair should both be in the same directory.
# eg. -I stm32cubemx/Core/Inc
AS_INC_FLAGS +=

# Miscellaneous Assembly flags
ASFLAGS += \
	-Wall -fdata-sections -ffunction-sections

######################### MISC #############################

# Libraries to link
LIBS +=

# Optimisation flags
OPT_FLAGS += -Og

# Debugging flags
DEBUG_C_FLAGS += -g -gdwarf-2

# Debugging defines
DEBUG_DEF_FLAGS += -DDEBUG