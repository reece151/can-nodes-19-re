
GDB_EXEC ?= /usr/bin/gdb-multiarch
export GDB_EXEC

PHONY: debug
debug: ## Build debug target, Launch GDB and connect to OpenOCD
debug: build/debug $(GDB_EXEC)
	$(QUIET)
	$(GDB_EXEC) -ex "target remote $$LOCALHOST:3333" -ex "load $(ELF_FILE)"
