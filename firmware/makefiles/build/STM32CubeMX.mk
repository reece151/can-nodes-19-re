# STM32CubeMX.mk - Makefile of targets related to working with the STM32CubeMX software and importing the existing generated Makefile
# We use STM32CubeMX to generate configuiration information and also to integrate our required STM32Cube HAL libraries
# Author(s): Reece Kibble

CUBEMX_APP_PATH ?= ~/STM32CubeMX/
CUBEMX_EXEC = STM32CubeMX

CUBEMXDIR=$(PROJECT_PATH)stm32cubemx/

override BUILD_DIR = build_defunct

# Include project .ioc file
include $(CUBEMXDIR)*.ioc

# Include generated Makefile
include $(CUBEMXDIR)Makefile

PROJECT_NAME=$(ProjectManager.ProjectName)
CUBEMX_CONFIG=$(CUBEMXDIR)$(ProjectManager.ProjectFileName)

# Generated makefile variable handling:
CUBEMX_VARS := TARGET DEBUG OPT MCU AS_DEFS C_DEFS AS_INCLUDES C_INCLUDES LDSCRIPT LIBS C_SOURCES ASM_SOURCES CFLAGS ASFLAGS LDFLAGS OBJECTS BUILD_DIR
define CUBEMX_VARSET_TEMPLATE
  $(eval CUBEMX_$(1) := $($(1)))
  $(eval $(1) :=)
endef
$(foreach var,$(CUBEMX_VARS),$(eval $(call CUBEMX_VARSET_TEMPLATE,$(var))))

C_DEF_FLAGS += $(CUBEMX_C_DEFS)
C_INC_FLAGS += $(addprefix -I$(CUBEMXDIR),$(subst -I,,$(CUBEMX_C_INCLUDES)))

AS_DEF_FLAGS += $(CUBEMX_AS_DEFS)
AS_INC_FLAGS += $(addprefix -I$(CUBEMXDIR),$(subst -I,,$(CUBEMX_AS_INCLUDES)))

CFLAGS += $(CUBEMX_MCU)
ASFLAGS += $(CUBEMX_MCU)
LDFLAGS += $(CUBEMX_MCU) 

LDSCRIPT := $(CUBEMXDIR)$(CUBEMX_LDSCRIPT)

LIBS += $(CUBEMX_LIBS)

C_SOURCES += $(addprefix $(CUBEMXDIR), $(CUBEMX_C_SOURCES))
AS_SOURCES += $(addprefix $(CUBEMXDIR), $(CUBEMX_ASM_SOURCES))

$(CUBEMXDIR)Makefile: $(CUBEMX_CONFIG)
	$(QUIET)
	$(RM) -f $(CUBEMXDIR)Makefile
	@echo '${FORMAT_BOLD}${YELLOW_T}STM32CubeMX Configuration .ioc modified. Generating project...${RESET}'
	$(MKDIR) -p $(GENERATEDDIR) $(LOGDIR)stm32cubemx
	echo "config load $(CUBEMX_CONFIG)" >> $(GENERATEDDIR)cubemx_generate.script 
	echo "project generate" >> $(GENERATEDDIR)cubemx_generate.script
	echo "exit" >> $(GENERATEDDIR)cubemx_generate.script
	java -jar $(CUBEMX_APP_PATH)$(CUBEMX_EXEC) -q $(GENERATEDDIR)cubemx_generate.script 2>$(LOGDIR)$@.log 1>$(LOGDIR)$@.log

$(CUBEMX_CONFIG):
	@echo '${FORMAT_BOLD}${RED_T}STM32CubeMX configuration .ioc file not found! Please create a project and save your configuration to: $@${RESET}'
	$(error )

$(LDSCRIPT):
	@echo '${FORMAT_BOLD}${RED_T}CubeMX Linker Script not found! Expected $@${RESET}'
	$(error )

.PHONY: stm32cubemx
stm32cubemx: ## Launch STM32CubeMX project in GUI (detaches from terminal)
stm32cubemx: $(LOGDIR)
	$(QUIET)
	nohup java -jar $(CUBEMX_APP_PATH)$(CUBEMX_EXEC) $(CUBEMX_CONFIG) 2>$(LOGDIR)$@.log 1>$(LOGDIR)$@.log &
	echo "Application log: $(LOGDIR)$@.log"
	echo "Remember: Dont generate code from within STM32CubeMX! Use 'make stm32cubemx/generate'"

.PHONY: stm32cubemx/generate
stm32cubemx/generate: ## Regenerate the STM32CubeMX project from the configuration .ioc file
stm32cubemx/generate: clean/stm32cubemx/Makefile stm32cubemx/check $(GENERATEDDIR) $(LOGDIR)
	$(QUIET)
	@echo '${FORMAT_BOLD}${YELLOW_T}Force regenerating project...${RESET}'
	echo "config load $(CUBEMX_CONFIG)" >> $(GENERATEDDIR)cubemx_generate.script 
	echo "project generate" >> $(GENERATEDDIR)cubemx_generate.script
	echo "exit" >> $(GENERATEDDIR)cubemx_generate.script
	java -jar $(CUBEMX_APP_PATH)$(CUBEMX_EXEC) -q $(GENERATEDDIR)cubemx_generate.script 2>$(LOGDIR)$@.log 1>$(LOGDIR)$@.log

.PHONY: stm32cubemx/check
stm32cubemx/check: ## Check the STM32CubeMX project has the correct settings
	@echo '${YELLOW_T}Checking STM32CubeMX .ioc Project Settings:${RESET}'
	@echo ''
	@echo 'Check ProjectManager.KeepUserCode is set to TRUE: $(if $(filter true,$(ProjectManager.KeepUserCode)),${GREEN_T}Success${RESET},${FORMAT_BOLD}${RED_T}Fail${RESET})'
	@echo 'Check ProjectManager.DeletePrevious is set to TRUE: $(if $(filter true,$(ProjectManager.DeletePrevious)),${GREEN_T}Success${RESET},${FORMAT_BOLD}${RED_T}Fail${RESET})'
	@echo 'Check ProjectManager.BackupPrevious is set to TRUE: $(if $(filter true,$(ProjectManager.BackupPrevious)),${GREEN_T}Success${RESET},${FORMAT_BOLD}${RED_T}Fail${RESET})'
	@echo 'Check ProjectManager.TargetToolchain is set to MAKEFILE: $(if $(filter Makefile,$(ProjectManager.TargetToolchain)),${GREEN_T}Success${RESET},${FORMAT_BOLD}${RED_T}Fail${RESET})'
	@echo ''
	@echo 'If any failures were detected please correct them before continuing!'

.PHONY: clean/stm32cubemx/Makefile
clean/stm32cubemx/Makefile: ## Delete the STM32CubeMX generated makefile (force regenerate)
	$(QUIET)
	$(RM) -f $(CUBEMXDIR)Makefile

SETTINGS_VARS += \
	CUBEMX_APP_PATH

 ##  