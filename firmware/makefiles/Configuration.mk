# Configuration.mk - Project-specific settings to be input by the user
# This makefile pulls functionality from all makefiles located in makefiles/configuration
# Author(s): Reece Kibble

## Generated README.md
GENERATED_README = $(GENERATED_SOURCE_DIR)README.md

CACHEDIR=$(PROJECT_PATH).config/
DIRS += $(CACHEDIR)

## Some generated files which describe CONFIGURATION_VARS
# A plain-text file, which could also be evaluated by a shell script if desired
SETTINGS_FILE = $(CACHEDIR)settings.txt

## Generate intermediate file which describes the configuration set via environment variables
# This file is generated on every build, even if the configuration has not changed
.PHONY: $(SETTINGS_FILE)
$(SETTINGS_FILE): |$(CACHEDIR)
	$(QUIET)
	# To the file $@:
	#  Print all configuration variables and their values, one per line
	(
	  true \
	  $(foreach v,$(sort $(SETTINGS_VARS)), \
		  $(if $($(v)),&& echo '$(v)=$($(v))') \
	   ) \
	) > $@

include $(SETTINGS_FILE)