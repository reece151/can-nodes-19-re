# Debug.mk - Makefile for debugging makefiles
# Author(s): Reece Kibble

# Define debugging macros;
#  If basig debugging of the Makefile(s) is needed, run `export DEBUG_MAKE=1`
#     You can use $(call debug, ...message) anywhere in any Makefile
#  If very detailed debugging the recipes used within the Makefiles is needed:
#    `export DEBUG_MAKE_SNATCH_SHELL=1`
#     This option causes a custom script called `snatch.sh` to be used instead of bash.
#     `snatch.sh` effectively wraps every shell command in some verbose logging
#     and calls to the `shellcheck` tool.

# The QUIET macro should be used for every recipe
# Under DEBUG_MAKE=1 it will report the target name and the recipe
# Under VERBOSE=1 the recipe is printed but not the target

## Export these debug settings so they are set for recursive Make processes
_DEBUG_SETTINGS_VARS := DEBUG_MAKE VERBOSE DEBUG_MAKE_SNATCH_SHELL SHOW_CHANGES

ifneq ($(DEBUG_MAKE),)
  define debug
    $(warning $(1))
  endef
  .SHELLFLAGS := -xec
#
  QUIET = $(call debug, In recipe for: $@)
#  DEBUG_MAKE_SNATCH_SHELL ?= 1
else
  debug :=
  # Control how verbose output appears; This command is prepended to recipes ONLY in VERBOSE mode
  # @set -v; is more convenient for the .ONESHELL
  VERBOSE_PREFIX ?= @set -v;
  # By default, $(QUIET) suppresses the printing of the recipe
  # By setting VERBOSE to either 1 or a specific target, the recipe will be shown according to VERBOSE_PREFIX
  QUIET = $(if $(filter 1 $@,$(VERBOSE)),$(VERBOSE_PREFIX),@$(if $(filter $(SHOW_CHANGES),1),echo $@ updating due to $? $$(test -f $@ && $(DATE) -r $@)))
endif

.PHONY: make-debug/%
make-debug/%: ## Run a target in debug mode. eg: make-debug/build
	$(QUIET)
	$(eval t=$(patsubst debug/%,%,$@))
	$(MAKE) ALLOW_RECURSIVE_MAKE=1 VERBOSE=$(t) VERBOSE_PREFIX="@set -xv;" $(t)

.PHONY: show/var/%
show/var/%: ## Display a make variable. eg: show/var/BUILDDIR
	$(QUIET)
	$(info $(notdir $@) = $($(notdir $@)))

 ## 