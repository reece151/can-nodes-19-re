# Terminal.mk - Terminal output helper macros
# Author(s): Reece Kibble

##### TPUT SETTINGS ##### 

# Text colours
BLACK_T := $(shell tput -Txterm setaf 0)
RED_T := $(shell tput -Txterm setaf 1)
GREEN_T := $(shell tput -Txterm setaf 2)
YELLOW_T := $(shell tput -Txterm setaf 3)
BLUE_T := $(shell tput -Txterm setaf 4)
MAGENTA_T := $(shell tput -Txterm setaf 5)
CYAN_T := $(shell tput -Txterm setaf 6)
WHITE_T := $(shell tput -Txterm setaf 7)

# Background colours
BLACK_H := $(shell tput -Txterm setab 0)
RED_H := $(shell tput -Txterm setab 1)
GREEN_H := $(shell tput -Txterm setab 2)
YELLOW_H := $(shell tput -Txterm setab 3)
BLUE_H := $(shell tput -Txterm setab 4)
MAGENTA_H := $(shell tput -Txterm setab 5)
CYAN_H := $(shell tput -Txterm setab 6)
WHITE_H := $(shell tput -Txterm setab 7)

# Formatting
FORMAT_BOLD := $(shell tput -Txterm bold)
FORMAT_DIM := $(shell tput -Txterm dim)
FORMAT_SMUL := $(shell tput -Txterm smul) # Underline
 
RESET  := $(shell tput -Txterm sgr0)