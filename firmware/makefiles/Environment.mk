# Build.mk - Makefile of targets for building/flashing the program
# This makefile pulls functionality from all makefiles located in makefiles/environment
# Author(s): Reece Kibble

# TODO - Implement the below plans:

# VSCode Interactivity - Targets to generate launch.json and tasks.json for better integration.
#	launch.json - generated to work with cortex-debug (maybe even automatically sourcing SVD?)
#	tasks.json - generated to include all makefile targets so they can be run without console

# GDB fixes - Add symlink for arm-none-eabi-gdb
# sudo ln -s /usr/bin/gdb-multiarch /usr/bin/arm-none-eabi-gdb