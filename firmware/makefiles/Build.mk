# Build.mk - Makefile of targets for building/flashing the program
# This makefile pulls functionality from all makefiles located in makefiles/build
# Author(s): Reece Kibble

# Build directories
BUILDDIR=$(PROJECT_PATH).build/
OBJECTSDIR=$(BUILDDIR)objects/
GENERATEDDIR=$(BUILDDIR)generated/
LOGDIR=$(BUILDDIR)log/
OUTPUTDIR=$(BUILDDIR)outputs/

# Define a gcc path here if your arm-none-eabi installation is non-standard
GCC_PATH=

include $(MAKEFILEDIR)build/STM32CubeMX.mk
include $(MAKEFILEDIR)build/GCC_Flags.mk
include $(MAKEFILEDIR)build/Openocd.mk
include $(MAKEFILEDIR)build/GDB.mk

# Output variables
OUT_NAME = $(OUTPUTDIR)$(PROJECT_NAME)$(VERSION)
ELF_FILE = $(OUT_NAME).elf
HEX_FILE = $(OUT_NAME).hex
BIN_FILE = $(OUT_NAME).bin
MAP_FILE = $(OUT_NAME).map

export ELF_FILE

CFLAGS += \
    $(OPT_FLAGS) \
    $(C_DEF_FLAGS) \
    $(C_INC_FLAGS)

ASFLAGS += \
    $(OPT_FLAGS) \
    $(AS_DEF_FLAGS) \
    $(AS_INC_FLAGS)

LDFLAGS += \
    -specs=nano.specs \
    -T$(LDSCRIPT) \
    $(LIBS) \
    -Wl,-Map=$(MAP_FILE),--cref \
    -Wl,--gc-sections \
	-Wl,--print-memory-usage

_C_SOURCES = $(sort $(C_SOURCES)) # Remove duplicates
C_OBJECTS = $(addprefix $(OBJECTSDIR), $(_C_SOURCES:.c=.o)) # Convert names to objects

_AS_SOURCES = $(sort $(AS_SOURCES)) # Remove duplicates
AS_OBJECTS = $(addprefix $(OBJECTSDIR), $(_AS_SOURCES:.s=.o)) # Convert names to objects

OBJECTS = $(C_OBJECTS) $(AS_OBJECTS)
NESTED_OBJECTS_DIRS = $(sort $(foreach o, $(OBJECTS), $(dir $(o)))) # Get directories

# GCC is able to generate the prerequiesite header files (.h) for C files after compiling them
# http://make.mad-scientist.net/papers/advanced-auto-dependency-generation/
DEPDIR := $(MAKEFILEDIR)dep/
# Convert object paths to depdir paths
OBJ2DEP = $(patsubst $(OBJECTSDIR)%, $(DEPDIR)%,$(1))
# Flags for gcc to produce dependancy file
DEPFLAGS = -MT $@ -MMD -MP -MF $(call OBJ2DEP, $@.Td)
# Precompile: Create the depdir if it doesnt exist
PRECOMPILE += $(if $(wildcard $(dir $(call OBJ2DEP, $@))),,$(MKDIR) -p $(dir $(call OBJ2DEP, $@)))
# Postcompile: Move temporary files to .mk files
POSTCOMPILE += $(MV) -f $(call OBJ2DEP, $@.Td) $(call OBJ2DEP, $@.mk)

BUILDDIRS += \
	$(GENERATEDDIR) \
	$(LOGDIR) \
	$(NESTED_OBJECTS_DIRS) \
	$(OUTPUTDIR) \
	$(DEPDIR)
BUILDDIRS := $(sort $(BUILDDIRS)) # Remove duplicates
DIRS += $(BUILDDIRS)


######################## TARGETS ##########################

# This function returns an include path that matches the .c/.s source
# This means automagic including when the header is in the same file as the c file (as recommended)
get_include_flags = -I $(dir $(1))

# Create objects from C source files
$(C_OBJECTS): $(OBJECTSDIR)%.o: %.c | $(NESTED_OBJECTS_DIRS) $(DEPDIR)
	$(QUIET)
	$(PRECOMPILE)
	@echo '${FORMAT_DIM}GCC - compiling file: $< to $@${RESET}'
	$(CC) $(CFLAGS) $(DEPFLAGS) $(call get_include_flags,$<) -Wa,-a,-ad,-alms=$(OBJECTSDIR)$(<:.c=.lst) -c -o $@ $<
	$(POSTCOMPILE)

# Create objects from Assembly source files
$(AS_OBJECTS): $(OBJECTSDIR)%.o: %.s | $(NESTED_OBJECTS_DIRS)
	$(QUIET)
	@echo '${FORMAT_DIM}AS - compiling file: $< to $@${RESET}'
	$(CC) $(ASMFLAGS) $(call get_include_flags,$<) -c -o $@ $<

# Link objects to ELF file
$(ELF_FILE): %.elf: $(OBJECTS) $(LDSCRIPT) | $(OUTPUTDIR)
	$(QUIET)
	@echo '${FORMAT_DIM}Linking .elf file: $@${RESET}'
	$(CC) -o $@ $(LDFLAGS) $(OBJECTS)

# Create Binary file
$(BIN_FILE): %.bin: $(ELF_FILE)
	$(QUIET)
	@echo '${FORMAT_DIM}Building .bin file: $@${RESET}'
	$(BIN) $< $@
	  
# Create Hex file compatible with flashing directly to device
$(HEX_FILE): %.hex: $(ELF_FILE)
	$(QUIET)
	@echo '${FORMAT_DIM}Building .hex file: $@${RESET}'
	$(HEX) $(ELF_FILE) $@

.PHONY: build
build: ## Build the program with default (release) flags
build: $(HEX_FILE)

.PHONY: build/debug
build/debug: ## Build the program with debugging flags/defines
build/debug: CFLAGS += $(DEBUG_C_FLAGS)
build/debug: CFLAGS += $(DEBUG_DEF_FLAGS)
build/debug: build
	@echo '${YELLOW_T}Compiled with debug flags: $(CFLAGS)${RESET}'

.PHONY: build/size
build/size: ## Displays detailed size information of the program
build/size:
	$(SZ) $(ELF_FILE)

.PHONY: clean/makefiles/dep
clean/makefiles/dep: ## Delete autodependancy makefile directory
	$(RM) -fR $(DEPDIR)

 ##  
